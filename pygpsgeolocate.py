#-------------------------------------------------------------------------------
# Name:        pygpsgeolocate
# Purpose:
#
# Author:      Philipp Nagel, City of Waseca MN
#
# Created:     23/03/2017
# Copyright:   (c) City of Waseca 2017
# Licence:     GNU General Public License v3.0
#
# Instructions
# 1. Install Python 2 from https://www.python.org/downloads/
# 2. Install pyserial and pynmea2:
#     pip install pyserial
#     pip install pynmea2
# 3. Antennaplus GPS is default COM Port 8, change port in script if different
# 4. Run script by double-clicking
# 5. In Firefox about:config, set the following:
#       geo.enabled: true
#       geo.provider.ms-windows-location: false
#       geo.provider.testing: true (have to add this entry)
#       geo.wifi.uri: http://localhost:8080
#
#-------------------------------------------------------------------------------

import serial
import string
import pynmea2
import json
from BaseHTTPServer import BaseHTTPRequestHandler,HTTPServer

import threading

# GPS Functions
# Set up GPS device serial port
ser = serial.Serial()
ser.port = "COM8"
ser.baudrate = 4800
ser.timeout = 5
ser.open()


# Get GPS Coordinates and return in JSON format
# Per HTML5 Geolocation
def GetLocation():
    loc = {}
    while not loc:
        data = ser.readline()
        # We only care about the GGA sentence
        # it contains coordinates
        if data[0:6] == '$GPGGA' or data[0:6] == '$GNGGA':
            gpgga = pynmea2.parse(data)
            if gpgga.latitude:
                loc['location'] = {}
                loc['accuracy'] = 5
                loc['location']['lat'] = gpgga.latitude
                loc['location']['lng'] = gpgga.longitude
                return json.dumps(loc)
            else:
                return False

def addLoc():
    global loco
    loco = GetLocation()
    threading.Timer(1.0, addLoc).start()

loco = {}
addLoc()

# Web Server Functions
PORT_NUMBER = 8080

err_mess = {"error": {"errors": [{"domain": "geolocation","reason": "notFound","message": "No GPS Location",}],"code": 404,"message": "No GPS Location"}}

class myHandler(BaseHTTPRequestHandler):
    def do_POST(self):
        # Next two lines seem required, otherwise malformed response
        content_len = int(self.headers.getheader('content-length', 0))
        post_body = self.rfile.read(content_len)
        # Send the actual response, code and headers first
        if loco:
            self.send_response(200)
            self.send_header('Content-type', 'application/json')
            self.send_header('Cache-Control:', 'no-cache, no-store, must-revalidate')
            self.send_header('Pragma:', 'no-cache')
            self.send_header('Expires:', 0)
            self.end_headers()
            self.wfile.write(loco)
            print loco
        else:
            self.send_response(404)
            self.send_header('Content-type', 'application/json')
            self.send_header('Cache-Control:', 'no-cache, no-store, must-revalidate')
            self.send_header('Pragma:', 'no-cache')
            self.send_header('Expires:', 0)
            self.end_headers()
            self.wfile.write(json.dumps(err_mess))
        return


try:
    server = HTTPServer(('', PORT_NUMBER), myHandler)
    print 'Started httpserver on port ', PORT_NUMBER
    server.serve_forever()

except KeyboardInterrupt:
    print 'Shutting down server'
    server.socket.close()
    print 'Closing Serial Port'
    ser.close()

