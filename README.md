This tool reads GPS location data from serial port (physical or virtual such as USB) connected GPS. It is useful to use GPS-derived location in a web browser, for example to show your location on a map.
GPS data must b in NMEA format.
I tested this in Windows 7 laptops, I am not sure if it will work on anything else.

Sets up HTTP server listening to location request from browser on port 8080. To use, change Firefox about:config settings as follows:
```
geo.enabled: true
geo.provider.ms-windows-location: false
geo.provider.testing: true
geo.wifi.uri: http://localhost:8080
```

Not figured out how to get it to work in other browsers.

When the browser requests geolocation, the script receives the POST request, which contains info such as Wifi APs and cellular towers, but ignores these. It simply returns the GPS coordinates in JSON format as it would be returned by Google's Geolocation API (documented here: https://developers.google.com/maps/documentation/geolocation/intro)

If no GPS coordinates are available (no fix, for example), the error "notFound	geolocation	404	The request was valid, but no results were returned." is returned to the browser.